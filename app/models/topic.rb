class Topic < ActiveRecord::Base
  belongs_to :user
  has_many :posts, dependent: :destroy

  validates :title, :content, :user_id, presence: true

  def as_json(options={})
    super().merge("posts" => self.posts)
  end

end
