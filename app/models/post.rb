class Post < ActiveRecord::Base
  belongs_to :user
  belongs_to :topic

  validates :user_id, :topic_id, :comment, presence: true

  def as_json(options={})
    super().merge({"created_at" =>  created_at.in_time_zone.strftime("%Y/%m/%d %H:%M:%S") ,"post_at"=> created_at.strftime("%d/%m/%y"), "user" => user.name})
  end
end
