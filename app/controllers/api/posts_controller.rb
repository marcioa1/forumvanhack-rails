class Api::PostsController < ApplicationController
    skip_before_filter :verify_authenticity_token, :only => :create

  def create
    @post = Post.new(post_params)
    if @post.save
      head :ok
    else
      head :bad_request
    end
  end

  private

  def post_params
    params.require(:post).permit(:user_id, :topic_id, :comment)
  end

end
