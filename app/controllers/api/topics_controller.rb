class Api::TopicsController < ApplicationController
  skip_before_filter :verify_authenticity_token

  def index
    @topics = Topic.all.order(created_at: :desc)
    render json: @topics.to_json
  end

  def create
    @topic = Topic.new(topic_params)
    if @topic.save
      head :ok
    else
      head :bad_request
    end
  end

  private

  def topic_params
    params.require(:topic).permit(:title, :content, :user_id)
  end
end
