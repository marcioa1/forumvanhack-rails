class Api::UsersController < ApplicationController

  skip_before_filter :verify_authenticity_token, :only => [:create, :login]

  def create
    @user = User.create(user_params)
    if @user.save
      render json: @user, status: :ok
    else
      render json: @user.errors.full_messages.join(", ").to_json, status: :bad_request
    end
  end

  def login
    @user = User.find_by email: params[:email]
    if @user
      if @user.valid_password?(params[:password])
        render json: @user, status: :ok
      else
        head :unauthorized
      end
    else
      head :not_found
    end
  end

  private

  def user_params
    params.require(:user).permit(:name, :email, :password, :password_confirmation)
  end
end
